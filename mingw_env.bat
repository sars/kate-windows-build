@echo off
setlocal

set PATH=C:\Qt\5.15.2\mingw81_64\bin;%PATH%
set PATH=C:\Qt\Tools\Ninja;%PATH%
set PATH="C:\Program Files\Git\usr\bin";%PATH%
set PATH=C:\Qt\Tools\mingw810_64\bin;%PATH%

set startTime=%time%
call %*
echo Start Time: %startTime%
echo Finish Time: %time%

endlocal
